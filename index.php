<?php

$dbh = new PDO('mysql:host=localhost;dbname=students', 'root', '');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: *');

// header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');

// header("Access-Control-Allow-Headers: X-Requested-With,Content-Type,Access-Control-Allow-Methods,Access-Control-Request-Headers");
// header("Access-Control-Allow-Headers: *");

// $students = json_decode( file_get_contents('students.json'), true );


// print_r($students);

// echo $students[0]['name'];

// print_r($_SERVER);

header('Content-Type: application/json; charset=UTF-8');

$method = $_SERVER['REQUEST_METHOD'];
$uri = explode('?', $_SERVER['REQUEST_URI'])[0];
$uri = substr($uri, 8 + 4);

$uri_ar = explode('/', $uri);

$resource = $uri_ar[0];
$resource_id = isset($uri_ar[1]) ? $uri_ar[1] : '';

if (!in_array($resource, ['students', 'groups'])) {
  http_response_code(404);
  echo json_encode(['message' => 'resource not found']);
  exit();
}

function InvalidRequired($value) {
    return !(isset($value) && $value);
}

function invalidMinLength($value, $minLength) {
    return count($value) < $minLength;
}

$models = [
    'students' => [
        'first_name' => [
            'type' => 'string',
            'required' => true,
            'minLength' => 2
        ],
        'last_name' => [
            'type' => 'string',
            'required' => true,
            'minLength' => 2
        ],
        'group_id' => [
            'type' => 'int',
            'required' => true,
        ],                
        'age' => [
            'type' => 'int',
            'required' => true,
        ],                
        'birthday' => [
            'type' => 'string',
            'required' => true,
        ],                
    ],
    'groups' => [
        'name' => [
            'type' => 'string',
            'required' => true,
            'minLength' => 2
        ],
    ],    
];

$model = $models[$resource];

function validateField($fieldRules, $value) {
    foreach($fieldRules as $rule_name => $rule_value) {
        switch ($rule_name) {
            case 'type':
                switch ($rule_value) {
                    case 'string':
                        return !is_string($value);
                    case 'int':
                        return !is_int($value);
                }
            case 'minLength':
                return minLength($value, $rule_value);
        }
    }
    return false;
}

function validateModel($model, &$values, $partial = false) {
    $errors_count = 0; 
    $new_values = [];
    if ($partial) {
        foreach($values as $fieldName => $value) {
            if (!isset($model[$fieldName])) {
                continue;
            }
            $new_values[$fieldName] = $values[$fieldName];
            $res = validateField($model[$fieldName], $value); 
            if ($res) {
                $errors_count++;
            }
        }
    } else {
        foreach($model as $fieldName => $fieldRules) {
            $new_values[$fieldName] = $values[$fieldName];
            if ($fieldRules['required'] && (!isset($values[$fieldName]) || !$values[$fieldName])) {
                $errors_count++;
                continue;
            }
            $res = validateField($fieldRules, $values[$fieldName]); 
            if ($res) {
                $errors_count++;
            }
        }
    }
    $values = $new_values;
}

function buildInsertRequest($resource, $model, $values) {
global $dbh;

    $fields = [];
    $_values = [];
    foreach($model as $fieldName => $fieldRules) {
        $fields[] = $fieldName;
        $_values[] = ':' . $fieldName;
    }
    $fieldsStr = implode(', ', $fields);
    $valuesStr = implode(', ', $_values);
    $q = "INSERT INTO {$resource} ({$fieldsStr}) VALUES ({$valuesStr})";
    // echo $q;
    $sth = $dbh->prepare($q);
    foreach($model as $fieldName => $fieldRules) {
        $sth->bindValue(':' . $fieldName, $values[$fieldName], $fieldRules['type'] === 'int' ? PDO::PARAM_INT : PDO::PARAM_STR);
    }    
    return $sth;
}

function buildUpdateRequest($resource, $model, $values, $id) {
global $dbh;
    $fields = [];
    foreach($values as $fieldName => $value) {
        $fields[] = "$fieldName = :$fieldName";
    }
    $fieldsStr = implode(', ', $fields);
    $q = "UPDATE {$resource} SET {$fieldsStr} WHERE id = :id";
    // echo $q;
    $sth = $dbh->prepare($q);
    foreach($values as $fieldName => $value) {
        $sth->bindValue(':' . $fieldName, $value, $model[$fieldName]['type'] === 'int' ? PDO::PARAM_INT : PDO::PARAM_STR);
    }    
    $sth->bindValue(':id', $id, PDO::PARAM_INT);
    return $sth;
}

switch ($method) {
    case 'GET':
        if (isset($resource_id) && $resource_id) {
            $sth = $dbh->prepare("SELECT * FROM {$resource} WHERE id = :id");
            $sth->bindValue(':id', $resource_id, PDO::PARAM_INT);
            $sth->execute();

            $student = $sth->fetch(PDO::FETCH_ASSOC);
            if ($student) {
                echo json_encode($student);
                exit();
            }
            http_response_code(404);
            echo json_encode(['message' => 'student not found']);
        } else {
            $where = '';
            $data = [];
            foreach($_GET as $key => $value) {
                if (isset($model[$key])) {
                    $where .= "AND $key = :{$key}";
                    $data[':'.$key] = $value;
                }
            }
            $sth = $dbh->prepare("SELECT * FROM {$resource} WHERE 1 = 1 $where");
            $sth->execute($data);
            
            $students = $sth->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($students);
        }
        break;
    case 'POST':
        $rawBody = file_get_contents('php://input');
        try {
            $data = json_decode($rawBody, true);
            $errors_count = validateModel($model, $data, false);
            if ($errors_count > 0) {
                http_response_code(400);
                echo json_encode(['message' => 'Bad request']);
                exit();
            }
            $sth = buildInsertRequest($resource, $model, $data);
            $sth->execute();

            // $dbh->lastInsertId()
            // $new_student = $sth->fetch(PDO::FETCH_ASSOC);

            $sth2 = $dbh->prepare("SELECT * FROM {$resource} WHERE id = :id");
            $sth2->bindValue(':id', $dbh->lastInsertId());
            $sth2->execute();
            $new_student = $sth2->fetch(PDO::FETCH_ASSOC);

            http_response_code(201);
            echo json_encode($new_student);
        } catch (Exception $e) {
            http_response_code(400);
            echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
        }
        break;
    case 'PUT':
        if (!isset($resource_id) || !$resource_id) {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }

        // print_r($student);
        $rawBody = file_get_contents('php://input');
        try {
            $data = json_decode($rawBody, true);
            $errors_count = validateModel($model, $data, true);
            if ($errors_count > 0) {
                http_response_code(400);
                echo json_encode(['message' => 'Bad request']);
                exit();
            }
            // print_r($data);
            $sth2 = $dbh->prepare("SELECT * FROM {$resource} WHERE id = :id");
            $sth2->bindValue(':id', $resource_id);
            $sth2->execute();
            $student = $sth2->fetch(PDO::FETCH_ASSOC);  
        
            if (!$student) {
                http_response_code(404);
                echo json_encode(['message' => 'student not found']);
                exit();
            }
            
            $sth = buildUpdateRequest($resource, $model, $data, $resource_id);
            $sth->execute();

            // echo "update affected rows:" . $sth->rowCount();

            $sth3 = $dbh->prepare("SELECT * FROM {$resource} WHERE id = :id");
            $sth3->bindValue(':id', $resource_id);
            $sth3->execute();
            $updated_student = $sth3->fetch(PDO::FETCH_ASSOC);

            http_response_code(200);
            echo json_encode($updated_student);
        } catch (Exception $e) {
            http_response_code(400);
            echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
        }
        exit();
        http_response_code(404);
        echo json_encode(['message' => 'student not found']);
        break;
    case 'DELETE':
        if (isset($resource_id) && $resource_id) {
            // $sth2 = $dbh->prepare("SELECT * FROM {$resource} WHERE id = :id");
            // $sth2->bindValue(':id', $resource_id);
            // $sth2->execute();
            // $student = $sth2->fetch(PDO::FETCH_ASSOC);  
        
            // if (!$student) {
            //     http_response_code(404);
            //     echo json_encode(['message' => 'student not found']);
            //     exit();
            // }

            $sth = $dbh->prepare("DELETE FROM {$resource} WHERE id = :id");
            $sth->bindValue(':id', $resource_id);
            $sth->execute();

            // echo "update affected rows:" . $sth->rowCount();
            if ($sth->rowCount() < 1) {
                http_response_code(404);
                echo json_encode(['message' => 'student not found']);
                exit();                
            }

            http_response_code(204);
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }
        break;
    default:
        http_response_code(400);
        echo json_encode(['message' => 'method is not supported']);
}
