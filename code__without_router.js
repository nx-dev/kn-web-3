var page = 'list';
var students = [];
var student = {};


function renderList() {
    document.getElementById('root').innerHTML = `
        <button id=newStudent>New</button><br/>
        total: <b id=totalResults></b><br>
        <input id=query name=query value="bitcoin"><button id=search>Search</button><br/>
        Current page: <span id=page>1</span> <button id=prev>Prev</button><button id=next>Next</button>
        <div id=students></div>
        <div id=note></div>
    `;
}

function renderEdit() {
    document.getElementById('root').innerHTML = `
        ${student.id}
        <button id=goToList>List</button><br/>
        First name <input id=first_name name=first_name value="${student.first_name}"><br/>
        Last name <input id=last_name name=last_name value="${student.last_name}"><br/>
        Group <input id=group_id name=group_id value="${student.group_id}"><br/>
        <div id=note></div>
        <button id=addStudent>Add</button>
    `;
}

function renderFormSuccess() {
    document.getElementById('root').innerHTML = `
        <button id=goToList>List</button><br/>
        Student added
    `;
}

function search() {
    // const url = `http://newsapi.org/v2/everything?page=${page}&q=${query}&from=2021-03-01&sortBy=publishedAt&apiKey=1529758ba1184a99b2d4bd370a82d491`;
    const url = `http://localhost/kn2018/api/students`;

    fetch(url).then(resp => {
        if (resp.status >= 200 && resp.status < 300) {
            return resp.json();
        } else {
            throw new Error(resp.statusText);
        }
    }).then((data) => {
        // document.getElementById('totalResults').innerText = data.totalResults;
        students = data;
        document.getElementById('students').innerHTML = processStudents(data);
        console.log(data)
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

function processStudents(students) {
    let html = '';
    students.forEach(student => {
        html += `
        <div class="news-block-wrapper">
            <a
                href=""
                data-ved="0ahUKEwjXrumh5JbvAhUxiYsKHQceBEEQxfQBCDMwAA"
                ping=""
            >
                <div class="news-block">
                    <div>
                        <div>
                            <div class="title" >${student.id} ${student.first_name} ${student.last_name}</div>
                            <div class="title" >${student.group_id}</div>
                        </div>
                    </div>
                    <div>
                        <button class=deleteItem data-id=${student.id}>Delete</button>
                        <button class=editItem data-id=${student.id}>Edit</button>
                    </div>
                </div>
            </a>
        </div>`
    });
    return html;
}
/*
document.getElementById('prev').addEventListener('click', () => {
    if (page === 1) return;
    page = page - 1;
    document.getElementById('page').innerText = page;
    search();
});
document.getElementById('next').addEventListener('click', () => {
    page = page + 1;
    document.getElementById('page').innerText = page;
    search();
});
document.getElementById('search').addEventListener('click', () => {
    query = document.getElementById('query').value;
    page = 1;
    document.getElementById('page').innerText = page;
    search();
});
*/
function deleteStudent(id) {
    var status;
    fetch('/kn2018/api/students/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        // body: ,
    }).then((resp) => {
        status = resp.status;
        if (status == 204) {
            return {};
        }
        return resp.json();
    }).then((data) => {
        console.log("🚀 ~ file: code.js ~ line 112 ~ addStudent ~ data", status, data)
        if (status >= 200 && status < 300) {
            search()
        } else {
            document.getElementById('note').innerText = data.message;
        }
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

function addOrEditStudent() {
    var data = {
        first_name: document.getElementById('first_name').value,
        last_name: document.getElementById('last_name').value,
        group_id: document.getElementById('group_id').value,
    }
    var status;
    fetch(`/kn2018/api/students/${student && student.id || ''}`, {
        method: student && student.id ? 'PUT' : 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    }).then((resp) => {
        status = resp.status;
        return resp.json();
    }).then((data) => {
        console.log("🚀 ~ file: code.js ~ line 112 ~ addStudent ~ data", status, data)
        if (status >= 200 && status < 300) {
            renderFormSuccess()
        } else {
            document.getElementById('note').innerText = data.message;
        }
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

document.addEventListener('click', (e) => {
    if (e.target.id === 'newStudent') {
        page = 'new';
        renderEdit();
    }
    if (e.target.id === 'goToList') {
        page = 'list';
        renderList();
        search();
    }
    if (e.target.id === 'addStudent') {
        addOrEditStudent();
    }
    if (e.target.classList.contains('deleteItem')) {
        var studentId = e.target.getAttribute('data-id');
        console.log("🚀 ~ file: code.js ~ line 165 ~ document.addEventListener ~ studentId", studentId)
        deleteStudent(studentId);
    }
    if (e.target.classList.contains('editItem')) {
        var studentId = e.target.getAttribute('data-id');
        var foundStudent = students.find(student => student.id = studentId);
        console.log("🚀 ~ file: code.js ~ line 176 ~ document.addEventListener ~ foundStudent", foundStudent)
        student =  foundStudent && {...foundStudent} || {};
        console.log('student', student)
        console.log("🚀 ~ file: code.js ~ line 165 ~ document.addEventListener ~ studentId", studentId)
        renderEdit();
    }
    
    e.preventDefault();
});

router();