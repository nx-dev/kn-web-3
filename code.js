var page = 'list';
var students = [];
var student = {};
var query = '';

function _404() {
    document.getElementById('root').innerHTML = '<h1>404 Not Found</h1>'
}

function _error(__error) {
    document.getElementById('root').innerHTML = `<h1>${__error}</h1>`
}

const prefix = '/kn2018';

function goToPage(path) {
    history.pushState({}, '', prefix + path);
    router();
}

function router() {
    const parts = location.pathname.split('/').slice(2);
    console.log("🚀 ~ file: code.js ~ line 7 ~ router ~ parts", parts.length, parts)
    if (parts.length === 3 & parts[0] === 'students' && parts[1] === 'edit') {
        student = {};
        page = 'edit';
        const id = parts[2];
        getStudent(id, (err, data) => {
            console.log("🚀 ~ file: code.js ~ line 28 ~ getStudent ~ err, data", err, data)
            if (err) {
                _error(`can't edit student with id ${id}`)
                return;
            }
            student = data;
            renderEdit();
        });        
        return;
    }
    if (parts.length === 2 && parts[0] === 'students' && parts[1] === 'new') {
        student = {};
        page = 'new';
        renderEdit();
        return;
    }
    console.log('1', parts, parts.length,  parts.length === 1, parts[0] === 'students', parts.length === 1 && parts[0] === 'students')
    if (parts.length === 1 && parts[0] === 'students') {
      renderList();
      search();
      return;
    }
    _404();
}

function renderList() {
    document.getElementById('root').innerHTML = `
        <button id=newStudent>New</button><br/>
        total: <b id=totalResults></b><br>
        <input id=query value=""><button id=search>Search</button><br/>
        Current page: <span id=page>1</span> <button id=prev>Prev</button><button id=next>Next</button>
        <div id=students></div>
        <div id=note></div>
    `;
}

function renderEdit() {
    document.getElementById('root').innerHTML = `
        ${student.id}
        <button id=goToList>List</button><br/>
        First name <input id=first_name name=first_name value="${student.first_name || ''}"><br/>
        Last name <input id=last_name name=last_name value="${student.last_name || ''}"><br/>
        Group <input id=group_id name=group_id value="${student.group_id || ''}"><br/>
        <div id=note></div>
        <button id=addStudent>Add</button>
    `;
}

function renderFormSuccess() {
    document.getElementById('root').innerHTML = `
        <button id=goToList>List</button><br/>
        Student added
    `;
}

function getStudent(id, cb) {
    const url = `http://localhost/kn2018/api/students/${id}`;
    fetch(url).then(resp => {
        if (resp.status >= 200 && resp.status < 300) {
            return resp.json();
        } else {
            throw new Error(resp.statusText);
        }
    }).then((data) => {
        console.log(data)
        cb(null, data)
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
        cb(err)
    })
}

function search() {
    // const url = `http://newsapi.org/v2/everything?page=${page}&q=${query}&from=2021-03-01&sortBy=publishedAt&apiKey=1529758ba1184a99b2d4bd370a82d491`;
    let url = `http://localhost/kn2018/api/students`;
    if (query) {
        url += `?first_name=${encodeURIComponent(query)}`
    }

    fetch(url).then(resp => {
        if (resp.status >= 200 && resp.status < 300) {
            return resp.json();
        } else {
            throw new Error(resp.statusText);
        }
    }).then((data) => {
        // document.getElementById('totalResults').innerText = data.totalResults;
        students = data;
        document.getElementById('students').innerHTML = processStudents(data);
        console.log(data)
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

function processStudents(students) {
    let html = '';
    students.forEach(student => {
        html += `
        <div class="news-block-wrapper">
            <a
                href=""
                data-ved="0ahUKEwjXrumh5JbvAhUxiYsKHQceBEEQxfQBCDMwAA"
                ping=""
            >
                <div class="news-block">
                    <div>
                        <div>
                            <div class="title" >${student.id} ${student.first_name} ${student.last_name}</div>
                            <div class="title" >${student.group_id}</div>
                        </div>
                    </div>
                    <div>
                        <button class=deleteItem data-id=${student.id}>Delete</button>
                        <button class=editItem data-id=${student.id}>Edit</button>
                    </div>
                </div>
            </a>
        </div>`
    });
    return html;
}
/*
document.getElementById('prev').addEventListener('click', () => {
    if (page === 1) return;
    page = page - 1;
    document.getElementById('page').innerText = page;
    search();
});
document.getElementById('next').addEventListener('click', () => {
    page = page + 1;
    document.getElementById('page').innerText = page;
    search();
});
*/

function deleteStudent(id) {
    var status;
    fetch('/kn2018/api/students/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        // body: ,
    }).then((resp) => {
        status = resp.status;
        if (status == 204) {
            return {};
        }
        return resp.json();
    }).then((data) => {
        console.log("🚀 ~ file: code.js ~ line 112 ~ addStudent ~ data", status, data)
        if (status >= 200 && status < 300) {
            search()
        } else {
            document.getElementById('note').innerText = data.message;
        }
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

function addOrEditStudent() {
    var data = {
        first_name: document.getElementById('first_name').value,
        last_name: document.getElementById('last_name').value,
        group_id: document.getElementById('group_id').value,
    }
    var status;
    fetch(`/kn2018/api/students/${student && student.id || ''}`, {
        method: student && student.id ? 'PUT' : 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    }).then((resp) => {
        status = resp.status;
        return resp.json();
    }).then((data) => {
        console.log("🚀 ~ file: code.js ~ line 112 ~ addStudent ~ data", status, data)
        if (status >= 200 && status < 300) {
            renderFormSuccess()
        } else {
            document.getElementById('note').innerText = data.message;
        }
    }).catch((err) => {
        console.log("🚀 ~ file: ajax.html ~ line 110 ~ fetch ~ err", err)
    })
}

document.addEventListener('click', (e) => {
    if (e.target.id === 'newStudent') {
        goToPage('/students/new');
    }
    if (e.target.id === 'goToList') {
        goToPage('/students');
    }
    if (e.target.id === 'addStudent') {
        addOrEditStudent();
    }
    if (e.target.id === 'search') {
        query = document.getElementById('query').value;
        // page = 1;
        // document.getElementById('page').innerText = page;
        search();
    }
    if (e.target.classList.contains('deleteItem')) {
        var studentId = e.target.getAttribute('data-id');
        console.log("🚀 ~ file: code.js ~ line 165 ~ document.addEventListener ~ studentId", studentId)
        deleteStudent(studentId);
    }
    if (e.target.classList.contains('editItem')) {
        var studentId = e.target.getAttribute('data-id');
        goToPage(`/students/edit/${studentId}`);
    }
    
    e.preventDefault();
});

router();