<?php

$students = json_decode( file_get_contents('students.json'), true );


// print_r($students);

// echo $students[0]['name'];

// print_r($_SERVER);

header('Content-Type: application/json; charset=UTF-8');

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$uri = substr($uri, 8);

$uri_ar = explode('/', $uri);

$resource = $uri_ar[0];
$resource_id = isset($uri_ar[1]) ? $uri_ar[1] : '';

if (!in_array($resource, ['students', 'groups'])) {
  http_response_code(404);
  echo json_encode(['message' => 'resource not found']);
  exit();
}

switch ($method) {
    case 'GET':
        if (isset($resource_id) && $resource_id) {
            foreach($students as $student) {
                if ($student['id'] == $resource_id) {
                    // print_r($student);
                    echo json_encode($student);
                    exit();
                }
            }
            http_response_code(404);
            echo json_encode(['message' => 'student not found']);
        } else {
            echo json_encode($students);
        }
        break;
    case 'POST':
        $rawBody = file_get_contents('php://input');
        try {
            $data = json_decode($rawBody, true);
            // print_r($data);
            if (!isset($data['name']) || !$data['name']) {
                http_response_code(400);
                echo json_encode(['message' => 'Bad request']);
                exit();
            }
            $student = [
                'id' => count($students) + 1,
                'name' => $data['name']
            ];
            $students[] = $student;
            file_put_contents('students.json', json_encode($students, null, 4));
            http_response_code(201);
            echo json_encode($student);
        } catch (Exception $e) {
            http_response_code(400);
            echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
        }
        break;
    case 'PUT':
        if (!isset($resource_id) || !$resource_id) {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }
        foreach($students as $key => $student) {
            if ($student['id'] == $resource_id) {
                // print_r($student);
                $rawBody = file_get_contents('php://input');
                try {
                    $data = json_decode($rawBody, true);
                    // print_r($data);
                    if (!isset($data['name']) || !$data['name']) {
                        http_response_code(400);
                        echo json_encode(['message' => 'Bad request']);
                        exit();
                    }
                    $student['name'] = $data['name'];
                    $students[$key] = $student;
                    file_put_contents('students.json', json_encode($students, null, 4));
                    http_response_code(200);
                    echo json_encode($student);
                } catch (Exception $e) {
                    http_response_code(400);
                    echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
                }
                exit();
            }
        }
        http_response_code(404);
        echo json_encode(['message' => 'student not found']);
        break;
    case 'DELETE':
        if (isset($resource_id) && $resource_id) {
            $new_students = [];
            foreach($students as $student) {
                if ($student['id'] != $resource_id) {
                    $new_students[] = $student;
                }
            }
            if (count($new_students) === count($students)) {
                http_response_code(404);
                echo json_encode(['message' => 'student not found']);
                exit();
            }
            file_put_contents('students.json', json_encode($new_students, null, 4));
            http_response_code(204);
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }
        break;
    default:
        http_response_code(400);
        echo json_encode(['message' => 'method is not supported']);
}
