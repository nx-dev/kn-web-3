<?php

$dbh = new PDO('mysql:host=localhost;dbname=students', 'root', '');



$students = json_decode( file_get_contents('students.json'), true );


// print_r($students);

// echo $students[0]['name'];

// print_r($_SERVER);

header('Content-Type: application/json; charset=UTF-8');

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$uri = substr($uri, 8);

$uri_ar = explode('/', $uri);

$resource = $uri_ar[0];
$resource_id = isset($uri_ar[1]) ? $uri_ar[1] : '';

if (!in_array($resource, ['students', 'groups'])) {
  http_response_code(404);
  echo json_encode(['message' => 'resource not found']);
  exit();
}

switch ($method) {
    case 'GET':
        if (isset($resource_id) && $resource_id) {
            $sth = $dbh->prepare("SELECT * FROM students WHERE id = :id");
            $sth->bindValue(':id', $resource_id, PDO::PARAM_INT);
            $sth->execute();

            $student = $sth->fetch(PDO::FETCH_ASSOC);
            if ($student) {
                echo json_encode($student);
                exit();
            }
            http_response_code(404);
            echo json_encode(['message' => 'student not found']);
        } else {
            $sth = $dbh->prepare("SELECT * FROM students");
            $sth->execute();
            
            $students = $sth->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($students);
        }
        break;
    case 'POST':
        $rawBody = file_get_contents('php://input');
        try {
            $data = json_decode($rawBody, true);
            if (!isset($data['first_name']) || !$data['first_name']) {
                http_response_code(400);
                echo json_encode(['message' => 'Bad request']);
                exit();
            }
            $sth = $dbh->prepare("INSERT INTO students (first_name, last_name, group_id) VALUES (:first_name, :last_name, :group_id)");
            $sth->bindValue(':first_name', $data['first_name'], PDO::PARAM_STR);
            $sth->bindValue(':last_name', $data['last_name'], PDO::PARAM_STR);
            $sth->bindValue(':group_id', $data['group_id'], PDO::PARAM_INT);
            $sth->execute();

            // $dbh->lastInsertId()
            // $new_student = $sth->fetch(PDO::FETCH_ASSOC);

            $sth2 = $dbh->prepare("SELECT * FROM students WHERE id = :id");
            $sth2->bindValue(':id', $dbh->lastInsertId());
            $sth2->execute();
            $new_student = $sth2->fetch(PDO::FETCH_ASSOC);

            http_response_code(201);
            echo json_encode($new_student);
        } catch (Exception $e) {
            http_response_code(400);
            echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
        }
        break;
    case 'PUT':
        if (!isset($resource_id) || !$resource_id) {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }

        // print_r($student);
        $rawBody = file_get_contents('php://input');
        try {
            $data = json_decode($rawBody, true);
            if (!isset($data['first_name']) || !$data['first_name']) {
                http_response_code(400);
                echo json_encode(['message' => 'Bad request']);
                exit();
            }
            $sth2 = $dbh->prepare("SELECT * FROM students WHERE id = :id");
            $sth2->bindValue(':id', $resource_id);
            $sth2->execute();
            $student = $sth2->fetch(PDO::FETCH_ASSOC);  
        
            if (!$student) {
                http_response_code(404);
                echo json_encode(['message' => 'student not found']);
                exit();
            }
            
            $sth = $dbh->prepare("UPDATE students SET first_name = :first_name, last_name = :last_name, group_id = :group_id WHERE id = :id");
            $sth->bindValue(':id', $resource_id);
            $sth->bindValue(':first_name', $data['first_name'], PDO::PARAM_STR);
            $sth->bindValue(':last_name', $data['last_name'], PDO::PARAM_STR);
            $sth->bindValue(':group_id', $data['group_id'], PDO::PARAM_INT);
            $sth->execute();

            // echo "update affected rows:" . $sth->rowCount();

            $sth3 = $dbh->prepare("SELECT * FROM students WHERE id = :id");
            $sth3->bindValue(':id', $resource_id);
            $sth3->execute();
            $updated_student = $sth3->fetch(PDO::FETCH_ASSOC);

            http_response_code(200);
            echo json_encode($updated_student);
        } catch (Exception $e) {
            http_response_code(400);
            echo json_encode(['message' => 'Bad request', 'debug_message' => $e->getMessage()]);
        }
        exit();
        http_response_code(404);
        echo json_encode(['message' => 'student not found']);
        break;
    case 'DELETE':
        if (isset($resource_id) && $resource_id) {
            // $sth2 = $dbh->prepare("SELECT * FROM students WHERE id = :id");
            // $sth2->bindValue(':id', $resource_id);
            // $sth2->execute();
            // $student = $sth2->fetch(PDO::FETCH_ASSOC);  
        
            // if (!$student) {
            //     http_response_code(404);
            //     echo json_encode(['message' => 'student not found']);
            //     exit();
            // }

            $sth = $dbh->prepare("DELETE FROM students WHERE id = :id");
            $sth->bindValue(':id', $resource_id);
            $sth->execute();

            // echo "update affected rows:" . $sth->rowCount();
            if ($sth->rowCount() < 1) {
                http_response_code(404);
                echo json_encode(['message' => 'student not found']);
                exit();                
            }

            http_response_code(204);
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'method is not supported']);
            exit();
        }
        break;
    default:
        http_response_code(400);
        echo json_encode(['message' => 'method is not supported']);
}
